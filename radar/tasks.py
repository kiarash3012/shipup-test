from copy import deepcopy
from math import radians, cos, sin, sqrt, atan2
from datetime import timedelta

from shipup.settings import Configs

from .models import UsersModel, PackagesModel
from .utils import CrawlPackageData, EmailNotification
from celery import task


class Notification:
    """
    this class creates desirable notifications for package shipments
    """
    def __init__(self, user, package_data, package_new_data):
        self.config = Configs.NotificationConfigs
        self.user = user
        self.package_data = package_data  # type: PackagesModel
        self.package_new_data = package_new_data  # type: PackagesModel
        self.arrival_change_text = None
        self.coordination_change_text = None
        self.template = None

    def movement_trigger(self):
        if self.package_new_data.longitude == self.package_data.longitude and\
                self.package_new_data.latitude == self.package_data.latitude:
            return False  # with radians calculations to equal points are the farthest points on a sphere :D
        lon1 = radians(self.package_new_data.longitude)
        lon2 = radians(self.package_data.longitude)
        lat1 = radians(self.package_new_data.latitude)
        lat2 = radians(self.package_data.longitude)
        # Haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1

        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        # Radius of earth in kilometers. Use 3956 for miles
        r = 6371

        return True if c * r > self.config.DISTANCE_THRESHOLD else False

    def arrival_change(self):
        diff_time = self.package_new_data.arrival_time - self.package_data.arrival_time
        if diff_time < timedelta(seconds=0):
            self.arrival_change_text = '({} Delay)'.format(str(diff_time))
        else:
            self.arrival_change_text = '({} in advance)'.format(str(diff_time))

    def coordinates_change(self):
        self.coordination_change_text = '[{}, {}] to [{}, {}]'.format(self.package_new_data.latitude, self.package_new_data.longitude,
                                                                      self.package_data.latitude, self.package_data.longitude)

    def send_email(self):
        if self.package_data.arrival_time is None:
            self.template = """Hi dear {}<br>Last status of airplane {}:<br>Time of Arrival: {}
            Current Location: [{}, {}]""".format(self.user.name, self.package_new_data.airplane,
                                                 self.package_new_data.arrival_time, self.package_new_data.latitude,
                                                 self.package_new_data.longitude)
        else:
            if self.package_data.arrival_time != self.package_new_data.arrival_time:
                self.arrival_change()
            if self.movement_trigger():
                self.coordinates_change()
            if self.coordination_change_text or self.arrival_change_text:
                self.template = """Hi dear {}<br>New update of airplane {} status:<br>Time of Arrival: {} """.format(
                    self.user.name, self.package_new_data.airplane, self.package_new_data.arrival_time)
                if self.arrival_change_text:
                    self.template += """({})""".format(self.arrival_change_text)
                if self.coordination_change_text:
                    self.template += """\nLocation: changed from {}""".format(self.coordination_change_text)

        if self.template:
            self.package_new_data.save()
            EmailNotification(self.user.email, self.template, 'package location', self.config.SOURCE_EMAIL).send()


@task()
def package_tracking():
    willing_users = UsersModel.objects.all().filter(package_tracking_notification=True)
    if willing_users:
        for user in willing_users:
            user_packages = PackagesModel.objects.all().filter(owner=user)
            for package_model in user_packages:
                package_data = deepcopy(package_model)
                updated_package_model = CrawlPackageData(package_id=package_data.id).crawl()

                Notification(user, package_data, updated_package_model).send_email()
