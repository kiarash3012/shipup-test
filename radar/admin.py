from django.contrib import admin

from .models import PackagesModel, UsersModel


class UsersModelAdmin(admin.ModelAdmin):
    list_display = ('email', 'package_tracking_notification')
    list_display_links = ('email', 'package_tracking_notification')
    list_filter = ('email', 'package_tracking_notification')


class PackagesModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'airplane', 'owner', 'arrival_time', 'latitude', 'longitude')
    list_display_links = ('id', 'airplane', 'owner', 'arrival_time', 'latitude', 'longitude')
    list_filter = ('id', 'airplane', 'owner', 'arrival_time', 'latitude', 'longitude')


admin.site.register(UsersModel, UsersModelAdmin)
admin.site.register(PackagesModel, PackagesModelAdmin)
