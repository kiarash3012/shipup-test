import json
import pytz

from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from datetime import datetime

from shipup.settings import Configs

from .models import PackagesModel


class EmailNotification:
    """
    you can send all emails with this util
    """
    def __init__(self, to_email, email_content, subject, from_email):
        self.to_email = to_email
        self.email_content = email_content
        self.subject = subject
        self.from_email = from_email
        self.configs = Configs.EmailConfigs

    def send(self):
        message = Mail(from_email=self.from_email, to_emails=self.to_email, subject=self.subject,
                       html_content='<strong>{}</strong>'.format(self.email_content))
        try:
            sg = SendGridAPIClient(self.configs.API_KEY)
            response = sg.send(message)
            print(response.status_code)
        except Exception as e:
            print(e.message)


class CrawlPackageData:
    """
    simulation 4 crawl service
    """
    def __init__(self, package_id):
        # todo : complete here to get data ...
        self.configs = Configs.CrawlConfigs
        self.package_id = package_id

    def get_package_data(self):
        with open(self.configs.DATA_SOURCE, 'r') as file_data:
            self.configs.DUMMY_CRAWL_COUNT += 1
            return json.loads(file_data.read())['data'][self.configs.DUMMY_CRAWL_COUNT]

    def crawl(self):
        package_model = PackagesModel.objects.filter(id=self.package_id).first()  # type: PackagesModel
        p_data = self.get_package_data()
        # todo: do serialization if necessary
        package_model.arrival_time = datetime.utcfromtimestamp(p_data['arrival_time']).replace(tzinfo=pytz.UTC)
        package_model.longitude = p_data['coordinates']['longitude']
        package_model.latitude = p_data['coordinates']['latitude']
        return package_model
