from django.db import models


class UsersModel(models.Model):
    name = models.TextField(max_length=100, null=True)
    email = models.EmailField(max_length=100, null=False, primary_key=True, default='root@root.com')
    package_tracking_notification = models.BooleanField(default=False)

    def __str__(self):
        return self.email


class PackagesModel(models.Model):
    id = models.AutoField(primary_key=True)
    airplane = models.TextField(max_length=1000, null=True, blank=True)
    owner = models.ForeignKey(UsersModel, on_delete=models.DO_NOTHING)
    arrival_time = models.DateTimeField(null=True, blank=True)
    latitude = models.FloatField(null=True, blank=True)
    longitude = models.FloatField(null=True, blank=True)

    def __str__(self):
        return self.airplane + '({})'.format(self.owner.email)

