SECRET_KEY = 'DSG433r&^*$#@21}{[]#@gsafjujad@!#!#f^5-^6@#t4&r+^mER$xc*#@%30z^zn'
# DEBUG = False
# ALLOWED_HOSTS = ['www.shipup.com', 'shipup.com]


#############################################################################################
class Configs:
    class EmailConfigs:
        API_KEY = ''

    class CrawlConfigs:
        DUMMY_CRAWL_COUNT = -1
        DATA_SOURCE = 'data/airplane_track_result.json'

    class NotificationConfigs:
        DISTANCE_THRESHOLD = 200  # min distance threshold to send notifications to customers
        SOURCE_EMAIL = 'support@shipup.com'


#############################################################################################
from celery.schedules import crontab
CELERY_BEAT_SCHEDULE = {
    "package-tracking-task": {
        "task": "radar.tasks.package_tracking",
        "schedule": crontab(hour='*/1')
    }
}

